<!DOCTYPE html>
<html>
    <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<style>
body, html {
  height: 100%;
  margin: 0;
   background-color: #0E5EFD;
}
.login_signup_box.clearfix.col-sm-offset-1 {
    margin-left: 0%!important;
}
.bgimg {
  background-color: #0E5EFD;
  height: 100%;
  background-position: center;
  background-size: cover;
  position: relative;
  color: white;
  font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif; 
  font-size: 25px;
}

.topleft {
  position: absolute;
  top: 50px;
  left: 16px;
}

.bottomleft {
  position: absolute;
  bottom: 0;
  left: 16px;
}

.middle {
    
   
  position: absolute;
  top: 40%;
  left: 50%;
  transform: translate(-50%, -50%);
  text-align: center;
}
.middle1 {
    width:70%;
  position: absolute;
  top: 70%;
  left: 50%;
  transform: translate(-50%, -50%);
  text-align: center;
}
.middle2 {
  position: absolute;
  top: 80%;
  left: 50%;
  transform: translate(-50%, -50%);
  text-align: center;
}
.bottomdown {
  position: absolute;
  top: 100%;
  left: 50%;
  transform: translate(-50%, -50%);
  text-align: center;
}
a
{
     font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif; 
}
#register
{
    width:140px;
    border-radius: 32px;
}
#signin
{
    width:140px;
    border-radius: 32px;
}
hr {
  margin: auto;
  width: 40%;
}
a#login_id {
    color: #fff;
}
a#login_id1 {
    color: #fff;
}
.text-center.term-links {
    font-size: 14px;
}
@media (min-width: 768px)
.col-sm-offset-1 {
    /* margin-left: 8.33333333%; */
}
.middle1
{
       /* border: 2px solid #fff;*/
}
            
            
</style>
<body>

<div class="bgimg" >
  <div class="topleft">
    <a href="http://bgl.v2web.in/chesslab/"><img src="1st_page/chess_title.png" style="width: 200px;"></a>
  </div>

  <br>
  <div class="middle1">
     <div class="login_signup_box clearfix col-sm-offset-1">

            <!--col-md-6-->
            <div class="col-md-5 col-sm-5" >
                <div class="login-getway" style="
    border: 2px solid #fff;
   
    padding:15px;
" >
                   
                     <h1> Log in</h1>
                      <div></div>
                    <div class="tab-content">
                        <div id="Buyer" class="tab-pane fade in active"> 
                            <form id="login" method="post" action="https://libazon.v2webcloud.com/site/login" novalidate="novalidate">

                                <div class="form-group">
                                    <input name="user_email" id="buyer_email" required="required" data-msg="Please enter email" type="email" placeholder="Email address" data-msg-email="Invalid email address" class="form-control valid" aria-required="true" aria-invalid="false">
                                </div>
                                <div class="form-group">
                                    <input name="user_password" id="user_password1" pattern=".{6,}" type="password" placeholder="Password" required="required" data-msg="Please enter password" class="form-control reg_newin valid" aria-required="true" aria-invalid="false">
                                </div> 
<!--                           <input type="submit" value="Sign me in!" name="login_btn" class="inputButton"/>-->
                                
                               <div class="form-group clearfix"> 
                            <div class="col-sm-6"> 
                                <input type="submit" value="Log in" name="login_btn" class="btn btn-warning" style="
    width: 100%;border-radius: 35px;
">
                            </div>
                            <div class="col-sm-6"> 
                                <a id="" class="btn btn-default" href="https://libazon.v2webcloud.com/site/password_new"  style="
    width: 100%;border-radius: 35px;
">Forgot password?</a>
                            </div>
                        </div>
                            </form>
                               
                        </div>
          
                    </div>
                </div>

  
            </div>

            <div id="signuphref" class="col-md-7 col-sm-7" style=" border: 2px solid #fff;
    padding-bottom: 0px;
    padding-top: 15px;
">
                <div class="signup-form">
                    <h1>Register</h1>
                    <div></div>
<!--                    <p class="col-sm-12">Start shopping with Libazon  </p>-->
                    <form id="signup" method="post" action="https://libazon.v2webcloud.com/site/signup" novalidate="novalidate">

                        <div class="form-group clearfix"> 
                            <div class="col-sm-6"> 
                                <input name="user_firstname" id="user_firstname" type="text" placeholder="First name" data-msg="Please enter first name" required="required" class="form-control reg_newin" aria-required="true">
                            </div>
                            <div class="col-sm-6"> 
                                <input name="user_lastname" id="user_lastname" required="required" data-msg="Please enter last name" type="text" placeholder="Last name" class="form-control " aria-required="true">
                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <div class="col-sm-12"> 
                                <input name="user_email" id="buyer_email" required="required" data-msg="Please enter email" type="email" placeholder="Email address" data-msg-email="Invalid email address" class="form-control valid" aria-required="true" aria-invalid="false">
                            </div>
                        </div>

                        <div class="form-group clearfix">
                            <div class="col-sm-12"> 
                                <input name="confemail" required="required" id="confemail" data-msg="Please enter confirm email" data-rule-equalto="#buyer_email" data-msg-equalto="Re-Enter Email not match" type="email" placeholder="Re-Enter email" data-msg-email="Invalid email address" class="form-control" onchange="confirmEmail();" aria-required="true">
                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <div class="col-sm-6"> 
                                <input name="user_password" id="user_password1" pattern=".{6,}" type="password" placeholder="Password" required="required" data-msg="Please enter password" class="form-control reg_newin valid" aria-required="true" aria-invalid="false">
                                <div id="message" style="display: none;">
                                <h4>Password must contain the following:</h4>
                                   
                                    
                                    <ul class="messagelist">
                                
                                    <li> 
                                    <p id="length" class="valid">Minimum <b>6 characters</b></p></li>
                                    </ul>
                            </div>
                            </div>

                            <div class="col-sm-6"> 
                                <input name="user_cpassword" type="password" placeholder="Confirm password" required="required" data-rule-equalto="#user_password1" data-msg-equalto="Confirm password not match" data-msg="Please enter confirm password" class="form-control " aria-required="true">
                            </div>
                        </div>
                        
                        <div class="form-group clearfix">
                            <div class="col-sm-6"> 

                                <input type="text" required="required" data-msg="Please enter phone number" placeholder="Phone number" name="user_contact" id="user_contact" value="" class="form-control reg_newin" aria-required="true">
                            </div>
                            <div class="col-sm-6"> 
                                <input type="text" required="required" placeholder="Street" data-msg="Please enter street" value="" name="user_shipping_Street" id="user_shipping_Street" class="form-control" aria-required="true">
                            </div>
                           
                            
                        </div>
                         <div class="form-group clearfix">
                         <div class="col-sm-12"> 
                                <input type="text" required="required" placeholder="Town" data-msg="Please enter town" value="" name="user_shipping_address2" id="user_shipping_address2" class="form-control " aria-required="true">
                            </div>
                            </div>
                        <div class="form-group clearfix">
                            <div class="col-sm-12"> 

<!--                                <input  placeholder="Address Line 1:  Building Number , Floor , Street Name" data-msg="Please enter address1" required="required" type="text" name="user_shipping_address1" value="" class="form-control m-bt-15">-->
                                <input placeholder="Address Line 1:  Floor, Building number/name" data-msg="Please enter address line" required="required" type="text" name="user_shipping_address1" id="user_shipping_address1" value="" class="form-control " aria-required="true">

                            </div>




                        </div> 

                        <!--                  <div class="form-group clearfix">
                                            <div class="col-sm-12"> 
                                              
                                               <input type="text" data-msg="Please enter city" required="required" placeholder="Please enter city"  name="user_city" value="" class="form-control">
                                            </div>
                                          </div>-->

                        <div class="form-group clearfix">
                            <div class="col-sm-6">
                            <div class="select-wrap"> 

                                <select class="user_city form-control" name="user_country" id="user_country" required="required" data-msg="Please select governorate" aria-required="true">
                                    <option value="">Select Governorate</option> 
                                                                            <option value="1">Beirut</option> 
                                                                            <option value="2">Bekaa</option> 
                                                                            <option value="3">Mount Lebanon</option> 
                                                                            <option value="4">Nabatieh</option> 
                                                                            <option value="5">North</option> 
                                                                            <option value="6">South</option> 
                                                                    </select> 
                                
                            </div>

                            </div>

                            <div class="col-sm-6"> 
                                <div class="select-wrap"> 
                                <select name="user_city" id="user_city" class="city_select custom-select form-control" required="required" data-msg="Please select district" aria-required="true">
                                    <option value="">Select District</option>
                                </select>
                                
                            </div>
                               
                            </div>
                        </div> 
                       
                        <div class="text-center term-links">
                            By registering, you agree to ChessLab <a href="#" style="color: yellow;"> Privacy Policy.</a>
                        </div>


                        <input type="submit" value="Register" name="signup_btn"  style="
    width: 30%;border-radius: 35px;
" class="btn btn-warning"> 

                </form>
                </div>
 <br>
                
            </div>

        </div>
    
  </div>
   
  </div>
  
<p style="height:100px"></p>

</body>
</html>
