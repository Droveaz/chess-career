<!DOCTYPE html>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body {
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}

.topnav {
  overflow: hidden;
  background-color: #0d5efc;
}


.topnav a {
  float: left;
  color: #f2f2f2;
  text-align: center;
  padding: 10px 16px;
  text-decoration: none;
  font-size: 14px;
}

.topnav a:hover {
  background-color: #ddd;
  color: black;
}

.box1 p {
    font-size: 14px;
}
.topnav a.active {
  background-color: #0d5efc;
  color: white;
}
.squares {
  display: flex;
  justify-content: center;
}
.icon1,
.icon2,
.icon3
{
    width: 150px;
    height: 150px;
    background: #0d5efc;
    color:#fff;
}


 .icon1 {
        margin-right: 0.5%;
        border-style: ridge;
        display: flex;
        align-items: center;
        justify-content: center;
    }   

 .icon2 {
        margin-right: 0.5%;
        border-style: ridge;
        display: flex;
        align-items: center;
        justify-content: center;
        }
        .icon3 
        {
        margin-right: 0.5%;
        border-style: ridge;
        display: flex;
        align-items: center;
        justify-content: center;
        }
        .logoimg
        {
            height:80px;
            
        }
                .iconoimg
        {
            height:42px;
            
        }
        
        .box1 {

    height: 150px;
    background-color: #0d5efc;
    margin-right: 0.5%;
    width: 153px;
    border: 1px solid #000;
    text-align: center;
    color:#fff;
}

</style>
</head>
<body>

<div class="topnav">
  <a class="active" href="#home">HOME</a>
  <a  href="aboutchess.php">ABOUT CHESS</a>
  <a href="news.php">NEWS</a>
  <a href="mediacenter.php">MEDIA CENTER</a>
   <a href="faq.php">FAQ</a>
   <a href="contactus.php">CONTACT US</a>
   
   <div class="topnav" style="float:right">
    <a href="http://chesscareer.com/signup">REGISTER</a>
</div>
</div>



<div style="padding-left:16px">
    <br>
    <center>
        <h2><img class="logoimg" src="colorlogo.png"></h2>
    </center>
 <br>

    <div class="row" style="
    margin-bottom: 5px;
">
     <div class="col-md-12">
     <div class="squares">
 <div class="box1">
     <br>
     <a href="http://chesscareer.com/
" style="
    text-decoration: none;
"><p><img class="iconoimg" src="icons/QUICK MODE.png"></p>
<p style="color:#fff;">QUICK MODE</p></a>
</div>
 <div class="box1">
      <br> <a href="http://chesscareer.com/
" style="
    text-decoration: none;
">
 <p><img class="iconoimg" src="icons/PLAY WITH FRIEND.png"></p>
<p style="color:#fff">PLAY A FRIEND</p></a>
</div>
 <div class="box1">
      <br>
      <a href="http://chesscareer.com/
" style="
    text-decoration: none;
">
  <p style="color:#fff" ><img class="iconoimg" src="icons/PLAY WITH AI.png"></p>
<p style="color:#fff">PLAY WITH AI</p></a>
</div>
</div>
</div>
</div>

   <div class="row">
     <div class="col-md-12">
     <div class="squares">
 <div class="box1">
      <br>
      <a href="http://chesscareer.com/
" style="
    text-decoration: none;
">
     <p><img class="iconoimg" src="icons/CAREER.png"></p>
<p style="color:#fff">CAREER</p></a>
</div>
 <div class="box1">
      <br>
      <a href=" http://chesscareer.com/login?referrer=/
" style="
    text-decoration: none;
">
 <p><img class="iconoimg" src="icons/PROFILE.png"></p>
<p style="color:#fff">PROFILE</p></a>
</div>
 <div class="box1">
      <br>
       <a href="wallet.php
" style="
    text-decoration: none;
">
  <p><img class="iconoimg" src="icons/WALLET.png"></p>
<p style="color:#fff">WALLET</p>
</a>
</div>
</div>
</div>
</div>
</body>
</html>
