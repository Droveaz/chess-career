<!DOCTYPE html>
<html>
    <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<style>
body, html {
  height: 100%;
  margin: 0;
 
}

.bgimg {
  background-color: #0E5EFD;
  height: 100%;
  background-position: center;
  background-size: cover;
  position: relative;
  color: white;
  font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif; 
  font-size: 25px;
}

.topleft {
  position: absolute;
  top: 0;
  left: 16px;
}

.bottomleft {
  position: absolute;
  bottom: 0;
  left: 16px;
}

.middle {
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  text-align: center;
}
.middle1 {
  position: absolute;
  top: 70%;
  left: 50%;
  transform: translate(-50%, -50%);
  text-align: center;
}
.middle2 {
  position: absolute;
  top: 80%;
  left: 50%;
  transform: translate(-50%, -50%);
  text-align: center;
}
a
{
     font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif; 
}
#register
{
    width:140px;
    border-radius: 32px;
}
#signin
{
    width:140px;
    border-radius: 32px;
}
#skip
{
    
        width: 83px;
    border-radius: 35px;
}
hr {
  margin: auto;
  width: 40%;
}
</style>
<body>

<div class="bgimg">
  <div class="topleft">
    
  </div>
  <div class="middle">
     <p><img src="1st_page/chess_title.png"></p>
    
  </div>
  <br>
  <div class="middle1">
      <p>
          <a href="register_signin.php" class="btn btn-default" id="register">Register</a> <a id="signin" href="register_signin.php" class="btn btn-default" >Signin</a>
         </p>
    
  </div>
     <div class="middle2">
    <p><a href="skip.php" class="btn btn-warning" id="skip">Skip</a></p>
  </div>
  
   
</div>


</body>
</html>
